use pdfium_render::prelude::*;
use std::path::{Path, PathBuf};

fn export_pdf_to_jpeg(
    source_files: &(PathBuf, &PathBuf, &String),
    password: Option<&str>,
) -> Result<(), PdfiumError> {
    // Renders each page in the given test PDF file to a separate JPEG file.

    // Bind to a Pdfium library in the same directory as our application;
    // failing that, fall back to using a Pdfium library provided by the operating system.
    let pp = match Path::new("/opt/pdfium-linux-x64").canonicalize() {
        Ok(path) => path,
        Err(e) => {
            panic!("No path found, {:#?}", e)
        }
    };
    let pdfium = Pdfium::new(
        Pdfium::bind_to_library(Pdfium::pdfium_platform_library_name_at_path(
            pp.to_str().unwrap().to_string(),
        ))
        .or_else(|_| Pdfium::bind_to_system_library())?,
    );
    // Open the PDF document...

    let pdf_file_path = source_files.0.to_str().unwrap();
    let jpeg_destination_folder_path = &source_files.1;
    let output_filename = source_files.2.as_str();

    let document = pdfium.load_pdf_from_file(pdf_file_path, password)?;

    // ... set rendering options that will apply to all pages...

    let bitmap_render_config = PdfBitmapConfig::new()
        .set_target_width(1200)
        .set_maximum_height(1200)
        .rotate_if_landscape(PdfBitmapRotation::Degrees90, true);

    // ... then render each page to a bitmap image, saving each image to a JPEG file.
    for (index, page) in document.pages().iter().enumerate() {
        let jpeg_path = jpeg_destination_folder_path
            .join(format!("{}-pag_{}.jpeg", output_filename, index + 1).as_str());
        let jpeg_path_str = jpeg_path.to_str().unwrap();
        // println!("{:#?}", &jpeg_path_str);
        page.get_bitmap_with_config(&bitmap_render_config)?
            .as_image() // Renders this page to an Image::DynamicImage...
            .as_rgba8() // ... then converts it to an Image::Image
            .ok_or(PdfiumError::ImageError)?
            .save_with_format(jpeg_path_str, image::ImageFormat::Jpeg)
            .map_err(|_| PdfiumError::ImageError)?;
        format!("&page");
    }
    Ok(())
}

fn choose_path(p: &Path, keyword: &str) -> PathBuf {
    let folder_fs: PathBuf = p
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .split(" ")
        .map(|x| x.to_string().to_lowercase())
        .filter(|y| y.contains(keyword))
        .map(|_| p)
        .collect();
    folder_fs
}

fn filter_path(p: &Vec<PathBuf>, name: Option<&str>) -> PathBuf {
    let hc_pdf: PathBuf = p
        .iter()
        .filter(|y| y.to_path_buf() == choose_path(&y, name.unwrap()))
        .collect();

    hc_pdf
}

fn get_subfolders(p: &Path) -> Vec<PathBuf> {
    let out_folders: Vec<PathBuf> = p
        .read_dir()
        .expect("read_dir call failed")
        .map(|x| match x {
            Ok(x) => x.path(),
            Err(e) => panic!("Error: {}", e),
        })
        .filter(|y| y.is_dir())
        .collect();

    out_folders
}

fn get_pdf_files(p: &Path) -> Vec<PathBuf> {
    let out_files: Vec<PathBuf> = p
        .read_dir()
        .expect("Folder not found")
        .map(|x| match x {
            Ok(x) => x.path(),
            Err(e) => panic!("Path Error: {:#?}", e),
        })
        .filter(|path| path.is_file())
        .collect();

    out_files
}

fn format_output_filenames(pdf_path: &Vec<PathBuf>) -> Vec<String> {
    let output_filename: Vec<String> = pdf_path
        .iter()
        .map(|path| {
            path.file_stem()
                .unwrap()
                .to_str()
                .unwrap()
                .replace(" ", "_")
        })
        .collect();

    output_filename
}

fn filter_filenames(file_names: &Vec<String>, keywords: &str) -> Vec<String> {
    let filenames: Vec<String> = file_names
        .into_iter()
        .filter(|&f| f.as_str().contains(keywords))
        .filter(|w| w.as_str() != "")
        .map(|f_st| f_st.to_owned())
        .collect();

    filenames
}

fn get_input_output(v_paths: &Vec<PathBuf>) -> Vec<(Vec<PathBuf>, Vec<PathBuf>, Vec<String>)> {
    // Output files, folders and filenames as per subfolder of parent folder's path
    let sb: Vec<(Vec<PathBuf>, Vec<PathBuf>, Vec<String>)> = v_paths
        .iter()
        .map(|vector_paths| {
            let paths: Vec<PathBuf> = get_subfolders(&vector_paths);
            let pdf_files: Vec<PathBuf> = get_pdf_files(&vector_paths);
            let output_filename: Vec<String> = format_output_filenames(&pdf_files);
            (pdf_files, paths, output_filename)
        })
        .collect();
    sb
}

fn get_source_files_path(source: &Vec<(Vec<PathBuf>, Vec<PathBuf>, Vec<String>)>) -> () {
    let _: () = source
        .iter()
        .map(|s| {
            let (pdf_files, jpeg_folder, output_filename) = s;

            let _: () = jpeg_folder
                .into_iter()
                .map(|folder_path| {
                    // Iterate over subfolders
                    let _ = match folder_path.file_name().unwrap().to_str().unwrap() {
                        "MICO_TACRO_jpeg" => {
                            let pdf_micofenolato = filter_path(&pdf_files, Some("mico"));
                            let mico_tacro_filename: Vec<String> =
                                filter_filenames(&output_filename, "mico");
                            let mico_source_files =
                                (pdf_micofenolato, folder_path, &mico_tacro_filename[0]);
                            let _: Result<(), PdfiumError> =
                                match export_pdf_to_jpeg(&mico_source_files, None) {
                                    Ok(()) => Ok(()),
                                    Err(e) => panic!("No image has been created, {:#?}", e),
                                };
                            println!("PDF coverted to JPEG: {:#?}", &mico_source_files.0)
                        }
                        "HC_jpeg" => {
                            let pdf_historia_clinica = filter_path(&pdf_files, Some("hist"));
                            let hc_filename: Vec<String> =
                                filter_filenames(&output_filename, "Hist");
                            let hc_source_files =
                                (pdf_historia_clinica, folder_path, &hc_filename[0]);
                            let _: Result<(), PdfiumError> =
                                match export_pdf_to_jpeg(&hc_source_files, None) {
                                    Ok(()) => Ok(()),
                                    Err(e) => panic!("No image has been created, {:#?}", e),
                                };
                            println!("PDF converted to JPEG: {:#?}", &hc_source_files.0)
                        }
                        _ => {}
                    };
                })
                .collect();
        })
        .collect();
}

fn pdf2_image(parent_folder_path: &Path) -> () {
    let jpeg_folders: Vec<PathBuf> = get_subfolders(parent_folder_path);
    let pdf_files = get_input_output(&jpeg_folders);
    let _ = get_source_files_path(&pdf_files);
}

fn main() -> Result<(), PdfiumError> {
    let base = Path::new("/run/media/carlosp81/DATA_A");

    let parent_folder = base
        .join("SERVICIOS")
        .join("MEDICOS")
        .join("RESULTADOS_GORDO")
        .join("citas_2022")
        .join("06-JUN_15");

    pdf2_image(&parent_folder);

    Ok(())
}
