use std::io::{self, Write};
fn main() {
    let sp: () = (0..2)
        .map(|x| {
            println!("=====================");
            println!(" Grades of Student {}", x + 1);
            println!("=====================");
            let mut total = 0.0;
            let _assi: () = (0..4)
                .map(|x| {
                    let mut grade: String = String::new();
                    println!("====* materia {} *====", x + 1);

                    print!("Insert your grade: {}", &grade);
                    match io::stdout().flush() {
                        Ok(_) => {}
                        Err(_) => panic!("Just flushed away user input"),
                    };
                    match io::stdin().read_line(&mut grade) {
                        Ok(n) => n,
                        Err(_) => {
                            panic!("Panic out")
                        }
                    };
                    // .expect("Insert a correct number");

                    let grade_number: f64 = match grade.trim().parse::<f64>() {
                        Ok(num) => num,
                        Err(_) => panic!("Err: Cannot Parse your grade"),
                    };
                    total += grade_number;
                    println!("Your current grade number is: {:.2}", &grade_number);
                    // println!("Your partial average grade is: {:#?}", &total / 4.0);
                })
                .collect();
            println!("Your final average_grade score is: {:.2}\n", &total / 4.0);
        })
        .collect();
    // .collect();
    println!("{:?}", &sp);
    println!("{:?}", std::mem::size_of_val(&sp));
}
