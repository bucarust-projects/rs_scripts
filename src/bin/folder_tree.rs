use std::fs;
use std::path::Path;

use std::io;
use std::io::Write;
fn main() -> std::io::Result<()> {
    let base = Path::new("/run/media/carlosp81/DATA_A");

    let fd_path = base
        .join("SERVICIOS")
        .join("MEDICOS")
        .join("RESULTADOS_GORDO")
        .join("citas_2022");

    println!("{:#?}", &fd_path);
    let mut folder_date = String::new();
    print!(
        "Add your appointment date folder:\t\t\t\t\t\t\t\t\t\t
        (decimalMonth-LiteralMonth_DayOfTheMonth) => (05-MAY_15): {}",
        &folder_date
    );
    match io::stdout().flush() {
        Ok(_) => {}
        Err(e) => {
            panic!("Warning by this error, {}", e);
        }
    };
    match io::stdin().read_line(&mut folder_date) {
        Ok(_) => {}
        Err(_) => {
            panic!("Panic out")
        }
    };

    let trim_folder_date = folder_date.trim();
    let folders: [&str; 3] = ["Historia_Clinica", "Laboratorios", "Ordenes_Medicas"];
    // let i: u8 = 0;
    folders.map(|folder| {
        let mut nested_folder = fd_path.join(trim_folder_date).join(folder);
        match folder {
            "Historia_Clinica" => {
                nested_folder = nested_folder.join("HC_jpeg");
            }
            "Laboratorios" => {
                nested_folder = nested_folder.join("Resultados");
            }
            _ => {
                nested_folder = nested_folder.join("MICO_TACRO_jpeg").join("AUTORIZACIONES");
            }
        };
        println!("{:?}", &nested_folder);
        match fs::create_dir_all(&nested_folder) {
            Ok(()) => {}
            Err(_) => {
                panic!("No folder has been created")
            }
        };
    });
    println!(
        "Your folder date created is {}\nlocation: {:#?}",
        &folder_date, &fd_path
    );
    Ok(())
}
